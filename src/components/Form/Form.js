import React from 'react';
import { Form, Input } from 'semantic-ui-react';
import InlineError from '../messages/InlineError';

class TodoForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      errors: {}
    };
  }

  handleChange = e => {
    e.preventDefault();
    this.setState({ value: e.target.value, errors: { name: '' } });
  };

  handleSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.value);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.props.addTodo(this.state.value);
      this.setState({ value: '' });
    }
  };

  validate = name => {
    const errors = {};
    if (!name) errors.name = "Name can't be blank";
    return errors;
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Field error={!!this.state.errors.name}>
          <Input
            onChange={this.handleChange}
            value={this.state.value}
            size="huge"
            action="Add"
            placeholder="Todo name..."
          />
          {this.state.errors && <InlineError text={this.state.errors.name} />}
        </Form.Field>
      </Form>
    );
  }
}

export default TodoForm;
