export const ADD_TODO = 'ADD_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const TOGGLE_IS_EDIT = 'TOGGLE_IS_EDIT';
export const UPDATE_TODO_VAL = 'EDIT_TODO_VAL';
