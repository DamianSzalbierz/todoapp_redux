import React from "react";
import TodoForm from "../Form/Form";
import TodoList from "../Todo/TodoList";
import { connect } from "react-redux";
import {
  updateTodo,
  saveTodo,
  removeTodo,
  toggleIsEdit,
} from "../../actions/index";

class TodoPage extends React.Component {
  render() {
    const {
      updateTodo,
      removeTodo,
      toggleIsEdit,
      saveTodo,
      todos
    } = this.props;

    return (
      <div>
        <TodoForm addTodo={saveTodo} />
        <TodoList
          updateTodo={updateTodo}
          toggleEdit={toggleIsEdit}
          todos={todos}
          deleteTodo={removeTodo}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({ todos: state.todos });

export default connect(mapStateToProps, {
  updateTodo,
  saveTodo,
  removeTodo,
  toggleIsEdit
})(TodoPage);
