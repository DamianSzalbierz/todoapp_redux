import React from "react";
import TodoItem from "./TodoItem";
import { List, Segment } from "semantic-ui-react";

class TodoList extends React.Component {
  render() {
    const todoList = this.props.todos.map(todo => (
      <TodoItem
        updateTodo={this.props.updateTodo}
        toggleEdit={this.props.toggleEdit}
        removeTodo={this.props.deleteTodo}
        key={todo.id}
        todo={todo}
      />
    ));
    return (
      <Segment raised>
        <List divided animated>
          {todoList}
        </List>
      </Segment>
    );
  }
}

export default TodoList;
