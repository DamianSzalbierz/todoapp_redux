import React from "react";
import { List, Header, Button, Form, Input } from "semantic-ui-react";

class TodoItem extends React.Component {
  state = {
    value: this.props.todo.value
  }

  handleDelete = () => {
    this.props.removeTodo(this.props.todo.id);
  };

  handleEdit = () => {
    this.props.toggleEdit(this.props.todo.id);
  };

  handleChange = e => {
    this.setState({value: e.target.value});
  }

  handleEditSubmit = (e) => {
    e.preventDefault();
    this.props.updateTodo(this.props.todo.id, this.state.value);
  }

  renderTodo = isEdited => {
    if (isEdited) {
      return (
        <Form onSubmit={this.handleEditSubmit}>
          <Form.Field>
            <Input action='save' value={this.state.value} onChange={this.handleChange} />
          </Form.Field>
          <Button
            onClick={this.handleEdit}
            floated="right"
            color="orange"
            content="Anuluj"
            icon="cancel"
          />
        </Form>
      )
    } else {
      return (
        <Header as="h3">
          <Header.Content>{this.props.todo.value}</Header.Content>
          <Button
            onClick={this.handleDelete}
            floated="right"
            color="red"
            content="usuń"
            icon="delete"
          />
          <Button
            onClick={this.handleEdit}
            floated="right"
            color="green"
            content="edytuj"
            icon="edit"
          />
        </Header>
      );
    }
  };

  render() {
    return (
      <List.Item>
        <List.Content>{this.renderTodo(this.props.todo.isEdited)}</List.Content>
      </List.Item>
    );
  }
}

export default TodoItem;
