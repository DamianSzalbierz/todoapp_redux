import { ADD_TODO, DELETE_TODO, TOGGLE_IS_EDIT, UPDATE_TODO_VAL } from "../lib/types";

const initialState = [
  { id: 0, value: "Pierwszy element mojej listy", isEdited: false },
  { id: 1, value: "Drugi element Twojej listy", isEdited: false },
  { id: 2, value: "Trzeci element mojej listy", isEdited: false },
  { id: 3, value: "Czwarty element Twojej listy", isEdited: false },
  { id: 4, value: "Piąty element mojej listy", isEdited: false },
  { id: 5, value: "Szósty element Twojej listy", isEdited: false },
  { id: 6, value: "Siódmy element mojej listy", isEdited: false },
  { id: 7, value: "Ósmy element Twojej listy", isEdited: false }
];

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_TODO:
      return [...state, action.payload];
    case DELETE_TODO:
      // action.payload is id
      return [...state.filter(todo => todo.id !== action.payload)];
    case TOGGLE_IS_EDIT:
      const todo = state.find(todo => action.payload === todo.id);
      return [
        ...state.slice(0, action.payload),
        { id: todo.id, value: todo.value, isEdited: !todo.isEdited },
        ...state.slice(todo.id + 1)
      ];
    case UPDATE_TODO_VAL:
      const updateTodo = state.find(todo => action.payload.id === todo.id);
      return [
        ...state.slice(0, action.payload.id),
        { id: updateTodo.id, value: action.payload.val, isEdited: !updateTodo.isEdited },
        ...state.slice(updateTodo.id + 1)
      ];
    default:
      return state;
  }
};
