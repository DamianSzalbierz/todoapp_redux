import { ADD_TODO, DELETE_TODO, TOGGLE_IS_EDIT, UPDATE_TODO_VAL } from '../lib/types';

let identity = 8;

export const saveTodo = (value) => {
  return {
    type: ADD_TODO,
    payload: {id: identity++, value}
  }
}

export const removeTodo = id => {
  return {
    type: DELETE_TODO,
    payload: id
  }
}

export const toggleIsEdit = id => {
  return {
    type: TOGGLE_IS_EDIT,
    payload: id
  }
}

export const updateTodo = (id, val) => {
  return {
    type: UPDATE_TODO_VAL,
    payload: {id: id, val: val}
  }
}
